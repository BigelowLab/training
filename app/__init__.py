# flask
from flask import Flask
from flask.ext.bootstrap import Bootstrap
from flask.ext.mail import Mail
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
from flask.ext.simpleldap import LDAP
# local
from config import config

bootstrap = Bootstrap()
mail = Mail()
db = SQLAlchemy()

login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'

def create_app(config_name):
    # create flask app
    app = Flask(__name__)
    # define configuration (i.e. testing, development, production)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)
    
    
    bootstrap.init_app(app) # init bootstrap
    mail.init_app(app)
    db.init_app(app) # init datahase
    login_manager.init_app(app )# init login manager
    
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix='/auth')

    from .expense import expense as expense_blueprint
    app.register_blueprint(expense_blueprint, url_prefix='/expense')

    from .training import training as training_blueprint
    app.register_blueprint(training_blueprint, url_prefix='/training')

    from .donations import donations as donations_blueprint
    app.register_blueprint(donations_blueprint, url_prefix='/donations')

    return app
