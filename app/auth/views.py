# auth/views.py renders and manages responses from the login and revister pages.

# flask
from flask import render_template, redirect, request, url_for, flash, current_app
from flask.ext.login import login_user, logout_user, login_required, \
    current_user
# other
from sqlalchemy import or_
# local
from . import auth
from .. import db
from ..models import User, Course, Training, member_of, user_exist, get_user_info
# from ..email import send_email
from .forms import LoginForm, RegistrationForm
import datetime
import ldap

def ldap_auth(username, password):
    try:
        # init ldap
        ld = ldap.initialize('ldap://ad01.bigelow.org')
        ld.protocol_version = 3
        ld.set_option(ldap.OPT_REFERRALS, 0)
        

        # check password - rebind with user
        ld.simple_bind_s(username+'@bigelow.org', password)

        # set up user query
        scope = ldap.SCOPE_SUBTREE
        attrs = ['*']
        base = 'dc=bigelow,dc=org'
        filter = '(&(objectClass=user)(sAMAccountName=' + username + '))'

        # search ldap for username
        results = ld.search(base, scope, filter, attrs)
        resp_type, user = ld.result(results, 60)
        name, attrs = user[0]

        return attrs
    except ldap.LDAPError, e:
        return False
    
    return False

def create_user(username, email, fullname):
    user = User(email=email,
                username=username.lower(), # convert to lower again just in case!
                full_name=fullname)
    db.session.add(user)
    db.session.commit()
    
    return user

# def update_passwd_length(user, passwd):
#     passwd = str(passwd)
#     user.passwd_length = int(len("passwd"))
#     db.session.add(user)
#     db.session.commit()
#
#     return passwd.len()

def add_training(uname):
    user = User.query.filter_by(username=uname).first()
    courses = Course.query.filter_by(required = 1).all()
    for course in courses:
        # create training for new user
        training = Training(user_id=user.id,
                                course_id=course.id,
                                year=datetime.datetime.now().year,
                                status=0,
                                location=1,
                                score=0,
                                questions=0)
        db.session.add(training)
    db.session.commit()
    
    return 1

# @auth.route('/email')
# def email():
#     send_email('kguay@bigelow.org', 'Confirm Your Account', 'auth/email/confirm', user=current_user)
#     flash('A message has been sent to you by email.')
#     return redirect(url_for('main.dashboard'))

# function that checks a username/ email and password (against the hashed
#  password stored in the database)
@auth.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
                
        # get ldap auth data
        ldap_result = ldap_auth(form.username.data,form.password.data)
        
        if ldap_result is not False:
            user = User.query.filter_by(username=form.username.data.lower()).first()
            
            # if user doest not exist in database: create it
            if user is None:
                
                email = ""
                if hasattr(ldap_result, 'has_key') and 'mail' in ldap_result:
                	email = ldap_result['mail'][0]
                
                fullname = ""
                if hasattr(ldap_result, 'has_key') and 'cn' in ldap_result:
                	fullname = ldap_result['cn'][0]
                
                username = form.username.data.lower()
                # if hasattr(ldap_result, 'has_key') and 'sAMAccountName' in ldap_result:
                    # username = ldap_result['sAMAccountName'][0]
                
                user = create_user(username, email, fullname)
                add_training(username)
                login_user(user, form.remember_me.data)
                return redirect(url_for('main.dashboard')) # used to be main.first
            
            print(len(form.password.data))
            # login
            # update_passwd_length(user, "pass")
            login_user(user, form.remember_me.data)
            return redirect(request.args.get('next') or url_for('main.index'))
        
        flash('Invalid username or password.')
        return render_template('auth/login.html', form=form)
        
    return render_template('auth/login.html', form=form)

def login_old():
    form = LoginForm()
    if form.validate_on_submit():
        
        user = User.query.filter((User.username==form.username.data) | (User.email==form.username.data)).first()
        
        if user is not None and user.verify_password(form.password.data):
            login_user(user, form.remember_me.data)
            return redirect(request.args.get('next') or url_for('main.index'))
        
        flash('Invalid username or password.')
        
    return render_template('auth/login.html', form=form)


# log a user out
@auth.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You have been logged out.')
    return redirect(url_for('main.index'))

# register a new user. 
@auth.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        # create new user
        user = User(email=form.email.data,
                    username=form.username.data,
                    full_name=form.full_name.data)
        db.session.add(user)
        #flash('You can now login.')
        return redirect(url_for('main.review'))
    return render_template('auth/register.html', form=form)

# register a new user. 
@auth.route('/ad_sync', methods=['GET', 'POST'])
def ad_sync():
        
    users_added = []
    for member in member_of('Phone Users'):
        # does the user exist?
        usr = User.query.filter_by(full_name=member).first()
        # if not, add user
        if usr is None:
            info = get_user_info(member)
            username=info[0][0]
            email=info[1][0]
            
            users_added.append(username)
            
            create_user(username, email, member)
            
    flash(users_added)

    return redirect(url_for('main.admin'))
