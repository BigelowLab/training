# views.py renders pages for creating, editing, reviewing, and approving
# expense reports.

import os
# flask
from flask.ext.login import current_user, logout_user, login_required
from flask_wtf.file import FileField
from flask import Flask, render_template, redirect, request, url_for, flash, send_from_directory, send_file
# other
from werkzeug import secure_filename
from datetime import datetime, date
from sqlalchemy import desc, asc, and_
# local
from . import donations
from .. import db
from ..decorators import admin_required, permission_required, advancement_required
from ..models import TransactionRequest


# log a user out
@donations.route('/')
@login_required
@advancement_required
def index():
    results_per_page = request.args.get('results')
    if results_per_page is not None:
        results_per_page = int(results_per_page)
    else:
        results_per_page = 10
    
    
    page = request.args.get('donation_page', 1, type=int)
    pagination = TransactionRequest.query.order_by('x_fp_timestamp desc').paginate(
        page, per_page=results_per_page,
        error_out=False)
    transactions = pagination.items
    
    return render_template('donations/index.html', transactions=transactions, pagination=pagination)
