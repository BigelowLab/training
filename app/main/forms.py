# forms.py is used to generate form fields that are used in the
# new_report.html and edit_report.html templates.

import re
# flask
from flask.ext.wtf import Form
from flask.ext.login import current_user
# other
from wtforms import ValidationError, StringField, DateField, DecimalField, FileField, SelectField, TextAreaField, FloatField, IntegerField, BooleanField, SubmitField
from wtforms.validators import Required
from datetime import datetime
from time import gmtime, strftime, localtime
# local
