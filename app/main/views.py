# views.py renders pages for creating, editing, reviewing, and approving
# expense reports.

import os
# flask
from flask.ext.login import current_user, logout_user, login_required
from flask_wtf.file import FileField
from flask import Flask, render_template, redirect, request, url_for, flash, send_from_directory, send_file
# other
from sqlalchemy import desc, asc, and_
# local
from . import main
from .. import db
from ..decorators import admin_required, create_required, permission_required
from ..models import member_of, belong_to, User, Training, Course, CourseContent, TransactionRequest

@main.route('/')
def index():
    if current_user.is_authenticated():
        return redirect(url_for('main.dashboard'))
    else:
        return redirect(url_for('auth.login'))

@main.route('/new_user')
def first():
    training_course_id = Training.query.filter_by(user_id=current_user.id).with_entities(Training.course_id)
    courses = Course.query.filter(Course.id.in_(training_course_id)).all()
    
    return render_template('new_user.html', courses=courses)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@main.route('/dashboard', methods=['GET','POST'])
@login_required
def dashboard():
    training = Training.query.filter_by(user_id=current_user.id).all()
    
    training_course_id = Training.query.filter_by(user_id=current_user.id).with_entities(Training.course_id)
    
    courses = Course.query.filter(Course.id.in_(training_course_id)).all()
    
    # courseinfo = Course.query.join(Training, Course.id==Training.course_id)
    
    # traininginfo = Training.query.join(Course, and_(Training.course_id==Course.id, Training.user_id=current_user.id))
    
    # DONATIONS
    page = request.args.get('donation_page', 1, type=int)
    pagination = TransactionRequest.query.order_by('x_fp_timestamp desc').paginate(
        page, per_page=5,
        error_out=False)
    transactions = pagination.items
    
    # flash(transactions[0].x_first_name)
        
    return render_template('dashboard2.html', training=training, courses=courses, transactions=transactions, pagination=pagination)


# Render FAQ page
@main.route('/faq')
@login_required
def faq():
    return render_template('faq.html')

@main.route('/admin', methods=['GET','POST'])
@create_required
def admin():
    # get everything
    courses = Course.query.join(User, Course.user).all()
    courseContent = CourseContent.query
    training = Training.query.join(User, Training.user).order_by('status').order_by('location').order_by(User.full_name).all()
    users = User.query.all()
    
    page = request.args.get('page', 1, type=int)
    pagination = TransactionRequest.query.order_by('x_fp_timestamp desc').paginate(
        page, per_page=5,
        error_out=False)
    transactions = pagination.items
    
    # transactions = TransactionRequest.query.order_by('x_fp_timestamp desc').limit(5).all()
    
    # selected_course = request.args.get('cid')
    # if selected_course is not None:
    #     courses = courses.filter_by(id=selected_course).all
    #     training = training.filter_by(course_id=selected_course).all
        
    # selected_user = request.args.get('uid')
    # if selected_user is not None:
    #     users = users.filter(id=selected_user)
    #     training = users.filter(user_id=selected_user)
    
    return render_template('admin.html', training=training, users=users, courses=courses, transactions=transactions, pagination=pagination, courseContent=courseContent)

# list users
@main.route('/users', methods=['GET'])
@create_required
def users():
    users = User.query.all()
    return render_template('users.html', users=users)

# toggle user manager
@main.route('/toggle_manager', methods=['GET','POST'])
@create_required
def toggle_manager():
    uid = request.args.get('uid')
    if uid is None:
        flash("User not found.")
        return redirect(url_for('main.users'))
    
    # get user
    user = User.query.filter_by(id=uid).first()
    # set manager flag
    user.manager = 1 - int(user.manager)
    db.session.add(user)
    db.session.commit()
    
    return redirect(url_for('main.users'))
    

# user information
@main.route('/user', methods=['GET','POST'])
@create_required
def user():
    uid = request.args.get('uid')
    if uid is None:
        flash("User not found.")
        return redirect(url_for('main.users'))
    
    try:
        training = Training.query.filter_by(user_id=uid).all()
        user = User.query.filter_by(id=uid).first()
    
        # users = member_of('Facilities')
        groups = belong_to(user.username)
    
        return render_template('user.html', user=user, training=training, groups=groups)
    except:
        flash("User not found.")
        return redirect(url_for('main.users'))
        
