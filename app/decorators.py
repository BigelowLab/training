# flask
from flask import abort
from flask.ext.login import current_user
# other
from functools import wraps
from .models import Permission


def permission_required(permission): 
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            if not current_user.iss_(permission): 
                abort(403)
            return f(*args, **kwargs) 
        return decorated_function
    return decorator 
    
def admin_required(f):
    return permission_required(Permission.ADMINISTER)(f)
    
def advancement_required(f):
    return permission_required(Permission.ADMINISTER)(f)
    
def create_required(f):
    return permission_required(Permission.CREATE)(f)
