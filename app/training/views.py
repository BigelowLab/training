# views.py renders pages for creating, editing, reviewing, and approving
# expense reports.

import os, glob
from os import path

# flask
from flask.ext.login import current_user, logout_user, login_required
from flask_wtf.file import FileField
from flask import current_app, Flask, render_template, redirect, request, jsonify, url_for, flash, send_from_directory, send_file
# other
from werkzeug import secure_filename
from datetime import datetime, date
from sqlalchemy import desc, asc, and_
# local
from . import training
from .forms import EditCourseHTMLForm, NewCourseForm, NewCourseContentForm, UploadImageForm, EditCourseName, NewCourseImagesForm
from .. import db
from ..decorators import admin_required, create_required, permission_required
from ..models import User, Training, Course, CourseContent

@training.route('/', methods=['GET','POST'])
@login_required
def index():
    
    # Course
    course_id = request.args.get('cid')
    if course_id is None:
        flash("Course not found.")
        return redirect(url_for('main.dashboard'))

    course_id = int(course_id)
    course = Course.query.filter_by(id=course_id).first()
    if course is None:
        flash("Course not found.")
        return redirect(url_for('main.dashboard'))
    
    # Training
    training = Training.query.filter_by(user_id=current_user.id).filter_by(course_id=course_id).first()
    # if the location is greater than 1 and status is still 0, change status to 1 (in progress)
    if training is None:
        training = Training(user_id=current_user.id,
                            course_id=course_id,
                            year=2016,
                            status=1,
                            location=1,
                            score=0,
                            questions=0)
        db.session.add(training)
    elif training.status == 0 and training.location > 0:
        training.status = 1
        db.session.add(training)
        db.session.commit()
    
    courseContentLength = CourseContent.query.filter_by(course_id=course_id).count()
    # Location
    loc = request.args.get('loc')
    if loc is not None:
        loc = int(loc)
    elif training.location >= courseContentLength:
        loc = 1
    else:
        loc = int(training.location)
    
    course = Course.query.filter_by(id=course_id).first()

    
    # if they try to skip ahead by editing the url, set loc to their last known location
    if loc > training.location+1:
        loc = training.location
    
    if loc == training.location+1:
        training.location = loc
        db.session.add(training)
    
    # get course content for current location
    courseContent = CourseContent.query.filter_by(course_id=course_id).filter_by(location=loc).first_or_404()
    courseContentAll = CourseContent.query.filter_by(course_id=course_id).order_by(CourseContent.location).all()
    
    
    # Score
    # init scores (can prob remove)
    score=0
    questions=0
    
    # init empty scores
    if training.score is None:
        training.score = 0
    if training.questions is None:
        training.questions = 0
    if courseContent.score is None:
        courseContent.score = 0
    if courseContent.total is None:
        courseContent.total = 0
    
    if request.method == "POST":
        score = int(request.form['score'])
        questions = int(request.form['questions'])
        loc = int(request.form['loc'])
        training.location = loc
        
        if int(score) is None:
            score = 0
        if int(questions) is None:
            questions = 0
        
        training.score = int(training.score) + int(score)
        courseContent.score = int(courseContent.score) + int(score)
        training.questions = int(training.questions) + int(questions)
        courseContent.total = int(courseContent.total) + int(questions)

        db.session.add(training)
        db.session.add(courseContent)
        db.session.commit()
    # End score
    
    if loc > course.length():
        training.status = 2
        db.session.add(training)
        db.session.commit()
        flash('complete')
        return redirect(url_for('main.dashboard'))
    
    courseContent = CourseContent.query.filter_by(course_id=course_id).filter_by(location=loc).first_or_404()
    courseContentLength = CourseContent.query.filter_by(course_id=course_id).count()

    
    return render_template('training/index.html',training=training, location=loc, course=course, courseContent=courseContent, courseContentAll=courseContentAll, courseContentLength=courseContentLength)

@training.route('/test', methods=['GET','POST'])
def test():
    if request.method == "POST":
        name = request.form['name']
        
        flash(name)
        
        return redirect(url_for('main.dashboard'))


@training.route('/say_name', methods=['POST'])
def say_name():
    
    flash(request.method)
    
    json = request.get_json()
    first_name = json['first_name']
    last_name = json['last_name']
    
    flash('hi')
    # return render_template('training/test.html')
    
    return jsonify(first_name=first_name, last_name=request.method)
    
    
@training.route('/overview', methods=['GET','POST'])
@create_required
def overview():
    cid = request.args.get('cid')
    if cid is None:
        flash("Course not found.")
        return redirect(url_for('main.admin'))
    
    training = Training.query.filter_by(course_id=cid).join(User, Training.user).order_by('status').order_by('location').order_by(User.full_name).all()
    
    # present = Training.query.filter_by(course_id=cid).with_entities(Training.user_id)
    # missing = User.query.filter(~(User.id._in(present))).all()
    
    return render_template('training/overview.html', training=training, course_id=cid)


# Create new course content
@training.route('/course_content', methods=['GET','POST'])
@login_required
def course_content():
    # Course
    course_id = request.args.get('cid')
    if course_id is None:
        flash("Course not found.")
        return redirect(url_for('main.dashboard'))

    course_id = int(course_id)
    course = Course.query.filter_by(id=course_id).first()
    
    if course is None:
        flash("Course not found.")
        return redirect(url_for('main.dashboard'))
    
    courseContent = CourseContent.query.filter_by(course_id=course_id).all()
    
    # Get current location in course and increment by one
    loc=1
    
    form = NewCourseContentForm()
    if form.validate_on_submit():
        courseContent = CourseContent(course_id=course_id,
                                      title=form.title.data,
                                      location=1,
                                      html=form.html.data)
        db.session.add(courseContent)
        db.session.commit()
        return redirect(url_for('main.dashboard'))

    return render_template('training/course_content.html', form=form, course=course, courseContent=courseContent)

# Create new course content
@training.route('/contents', methods=['GET','POST'])
@login_required
def contents():
    # Course
    course_id = request.args.get('cid')
    if course_id is None:
        flash("Course not found.")
        return redirect(url_for('main.dashboard'))

    course_id = int(course_id)
    course = Course.query.filter_by(id=course_id).first()
    
    if course is None:
        flash("Course not found.")
        return redirect(url_for('main.dashboard'))
    
    courseContent = CourseContent.query.filter_by(course_id=course_id).order_by(CourseContent.location).all()
    courseContentLength = CourseContent.query.filter_by(course_id=course_id).count()
    
    # Get current location in course and increment by one
    loc=1
    
    form = NewCourseContentForm()
    if form.validate_on_submit():
        courseContent = CourseContent(course_id=course_id,
                                      title=form.title.data,
                                      location=1,
                                      html=form.html.data)
        db.session.add(courseContent)
        db.session.commit()
        return redirect(url_for('main.dashboard'))
        
    
    # should we edit the course title?

    editTitle = request.args.get('edit')
    if editTitle is not None:
        editTitle = True
    else:
        editTitle = False
    # edit course title
    editCourseName = EditCourseName()
    if editCourseName.validate_on_submit():
        course.name = editCourseName.name.data
        db.session.add(course)
        db.session.commit()
        course = Course.query.filter_by(id=course_id).first()
        return redirect(url_for('training.contents', cid=course_id))
    editCourseName.name.data = course.name
    
    return render_template('training/contents.html', form=form, course=course, courseContent=courseContent, courseContentLength=courseContentLength, editTitle=editTitle, editCourseName=editCourseName)

# Preview a course
@training.route('/edit', methods=['GET','POST'])
@login_required
def edit():
    
    # Course
    course_id = request.args.get('cid')
    if course_id is None:
        flash("Course not found.")
        return redirect(url_for('main.dashboard'))

    course_id = int(course_id)
    course = Course.query.filter_by(id=course_id).first()
    if course is None:
        flash("Course not found.")
        return redirect(url_for('main.dashboard'))
    
    # Location
    loc = request.args.get('loc')
    if loc is not None:
        loc = int(loc)
    else:
        loc = 1
    
    courseContent = CourseContent.query.filter_by(course_id=course_id).filter_by(location=loc).first_or_404()
    
    
    # Edit
    edit = request.args.get('edit')
    if edit is not None:
        edit = True
    else:
        edit = False
    
    # edit HTML code in database
    form = EditCourseHTMLForm()
    if form.validate_on_submit():
        courseContent.title = form.title.data
        
        if int(form.seconds.data) is not None and int(form.seconds.data) > 0:
            courseContent.seconds = int(form.seconds.data)
        else:
            courseContent.seconds = 0
        
        courseContent.html = form.html.data
        courseContent.updated_by = current_user.id
        db.session.add(courseContent)
        db.session.commit()
        return redirect(url_for('training.edit', loc=loc, cid=course_id))
    
    courseContent = CourseContent.query.filter_by(course_id=course_id).filter_by(location=loc).first_or_404()
    courseContentLength = CourseContent.query.filter_by(course_id=course_id).count()
    courseContentAll = CourseContent.query.filter_by(course_id=course_id).order_by(CourseContent.location).all()
    form.html.data = courseContent.html
    form.title.data = courseContent.title
    form.seconds.data = courseContent.seconds
    
    # Images
    dir = os.path.join(current_app.config['UPLOAD_FOLDER'], str(current_user.username))
    os.chdir(dir)
    images = glob.glob("*.jpg")
    
    return render_template('training/edit.html', course=course, courseContent=courseContent, courseContentLength=courseContentLength, courseContentAll=courseContentAll, location=loc, edit=edit, form=form, images=images)

# Create a new course
@training.route('/new_course', methods=['GET','POST'])
@login_required
def new_course():

    course = Course(name="Untitled Course", created_by=current_user.id, updated_by=current_user.id)
    db.session.add(course)
    db.session.commit()
    
    course = Course.query.order_by(Course.id.desc()).first_or_404()
    
    flash('Course created')
    return redirect(url_for("training.contents", cid=course.id))


# Create a new course page
@training.route('/new_page', methods=['GET','POST'])
@login_required
def new_page():
    cid=request.args.get('cid')
    if cid is not None:
        last_page = CourseContent.query.filter_by(course_id=cid).order_by(CourseContent.location.desc()).first()
        if last_page is not None:
            next_location = last_page.location + 1
        else:
            next_location = 1
        
        courseContent = CourseContent(title="Untitled", course_id=cid, location=next_location, created_by=current_user.id, updated_by=current_user.id, seconds=0)
        db.session.add(courseContent)
        db.session.commit()

        flash('Page created')
        return redirect(url_for("training.contents", cid=cid))

    flash('No course selected')
    return redirect(url_for('training.contents'))


# Preview a course
@training.route('/move_page', methods=['GET','POST'])
@login_required
def move_page():
    
    # Course
    course_id = request.args.get('cid')
    if course_id is None:
        flash("Course not found.")
        return redirect(url_for('training.contents'))

    course_id = int(course_id)
    course = Course.query.filter_by(id=course_id).first()
    if course is None:
        flash("Course not found.")
        return redirect(url_for('training.contents'))
    
    # Location
    loc = request.args.get('loc')
    if loc is not None:
        loc = int(loc)
    else:
        loc = 1
    
    this = CourseContent.query.filter_by(course_id=course_id).filter_by(location=loc).first_or_404()
    
    # Move
    direction = request.args.get('dir')
    if direction == 'up':
        next = CourseContent.query.filter_by(course_id=course_id).filter_by(location=loc-1).first_or_404()
        temp = this.location
        this.location = next.location
        next.location = temp
    else:
        last = CourseContent.query.filter_by(course_id=course_id).filter_by(location=loc+1).first_or_404()
        temp = this.location
        this.location = last.location
        last.location = temp
    
    return redirect(url_for('training.contents', cid=course_id, loc=loc))


@training.route('/upload', methods=['GET','POST'])
@login_required
def upload():
    form = UploadImageForm()
    
    if form.validate_on_submit():
        uploaded_files = request.files.getlist('file')
        
        dir = os.path.join(current_app.config['UPLOAD_FOLDER'], str(current_user.username))
        
        if not (os.path.isdir(dir)):
            os.makedirs(dir)
            
        for file in uploaded_files:
            file.save(os.path.join(dir, file.filename))
        
    return render_template('training/upload.html', form=form)



@training.route('/course_from_images', methods=['GET','POST'])
@login_required
def course_from_images():
    form = NewCourseImagesForm()
    
    if form.validate_on_submit():
        uploaded_files = request.files.getlist('file')
        course_name = form.name.data
        
        dir = os.path.join(current_app.config['UPLOAD_FOLDER'], str(current_user.username))
        
        if not (os.path.isdir(dir)):
            os.makedirs(dir)
        
        # create new course
        course = Course(name=course_name, created_by=current_user.id, updated_by=current_user.id)
        db.session.add(course)
        
        db.session.commit()
        course = Course.query.order_by(Course.id.desc()).first_or_404()
        
        # get next page location
        last_page = CourseContent.query.filter_by(course_id=course.id).order_by(CourseContent.location.desc()).first()
        if last_page is not None:
            next_location = last_page.location + 1
        else:
            next_location = 1
        
        page_number = 1 # counter for course name
        for file in uploaded_files:
            filename = os.path.join(dir, file.filename)
            # save file
            file.save(filename)
            # create new page 
            
            courseContent = CourseContent(title="Page "+str(page_number), course_id=course.id, location=next_location, html="<img src='" + url_for('static', filename=os.path.join("training/user_images", str(current_user.username),file.filename)) + "' width='75%'>", created_by=current_user.id, updated_by=current_user.id)
            db.session.add(courseContent)
            db.session.commit()
            
            # increment counters
            next_location = next_location + 1
            page_number = page_number + 1
        
        return redirect(url_for("training.contents", cid=course.id))
        
    return render_template('training/course_from_images.html', form=form)

@training.route('/files', methods=['GET','POST'])
@login_required
def files():
    
    dir = os.path.join(current_app.config['UPLOAD_FOLDER'], str(current_user.id))
    os.chdir(dir)
    files = glob.glob("*.jpg")
        
    return render_template('training/files.html', files=files, dir=dir)


@training.route('/delete_course_content', methods=['GET','POST'])
@login_required
def delete_course_content():
    cid=request.args.get('cid')
    loc=request.args.get('loc')
    if cid is not None and loc is not None:
        # load the expense form id (if one exists)
        courseContent = CourseContent.query.filter_by(course_id=cid).filter_by(location=loc).first_or_404()
        # delete row
        db.session.delete(courseContent)
        # update table
        db.session.commit()
        
        # decrement location for pages with higher location than deleted page
        pages = CourseContent.query.filter_by(course_id=cid).filter(CourseContent.location > loc).all()
        # loop through pages with location > loc and shift each down one.
        for page in pages:
            page.location = page.location - 1
    
    flash("Page deleted")
    return redirect(url_for('training.contents', cid=cid))

@training.route('/delete_course', methods=['GET','POST'])
@login_required
def delete_course():
    cid=request.args.get('cid')
    if cid is not None:
        course = Course.query.filter_by(id=cid).first_or_404()
        # delete row
        db.session.delete(course)
        
        # decrement location for pages with higher location than deleted page
        pages = CourseContent.query.filter_by(course_id=cid).all()
        # loop through pages with location > loc and shift each down one.
        for page in pages:
            db.session.delete(page)
    
    flash("Course deleted")
    return redirect(url_for('main.admin'))
