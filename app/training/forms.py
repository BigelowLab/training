# forms.py is used to generate form fields

import re
# flask
from flask.ext.wtf import Form
from flask.ext.login import current_user
# other
from wtforms import ValidationError, StringField, IntegerField, TextAreaField, FileField, SubmitField
from wtforms.validators import Required
from datetime import datetime
from time import gmtime, strftime, localtime
# local
from ..models import CourseContent, Course

class EditCourseHTMLForm(Form):
    title = StringField('Title', validators=[Required()])
    seconds = StringField('Time')
    html = TextAreaField('')
    submit = SubmitField('Save')

class EditCourseName(Form):
    name = StringField('Name')
    submit = SubmitField('Save')

class NewCourseForm(Form):
    name = StringField('Name', validators=[Required()])
    submit = SubmitField('Create')

class NewCourseContentForm(Form):
    title = StringField('Title', validators=[Required()])
    html = TextAreaField('HTML Content', validators=[Required()])
    submit = SubmitField('Save')

class UploadImageForm(Form):
    file = FileField('File', validators=[Required()])
    submit = SubmitField('Upload')

class NewCourseImagesForm(Form):
    name = StringField('Course Name', validators=[Required()])
    file = FileField('Images', validators=[Required()])
    submit = SubmitField('Create')
