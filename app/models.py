# flask
from flask import current_app
from flask.ext.login import UserMixin
# local
from . import db, login_manager

import ldap


class Permission:
    ADVANCEMENT = ["OUs","Advancement"]
    COMMUNICATIONS = ["OUs","Communications"]
    EDUCATION = ["OUs","Education"]
    FINANCE = ["OUs","Finance"]
    FACILITIES = ["OUs","Facilities"]
    SAFETY = ["OUs","Safety Committee"]
    HR = ["OUs","HR"]
    IT = ["OUs","IT"]
    SRS = ["OUs","SRS"]
    
    ADMINISTER = IT
    CREATE = ADMINISTER, FACILITIES


def check_group(username, org, grp):
    # init ldap
    ld = ldap.initialize('ldap://ad01.bigelow.org')
    ld.protocol_version = 3
    ld.set_option(ldap.OPT_REFERRALS, 0)

    # check password - rebind with user
    ld.simple_bind_s('pythonauth'+'@bigelow.org', '4#Btb%>84==2jR3Y6z4Z=Vvke2j{4NVNHKDLcWXrCwhF9a((s>')

    # set up user query
    scope = ldap.SCOPE_SUBTREE
    attrs = ['*']
    base = 'dc=bigelow,dc=org'
    filter = '(&(objectClass=user)(sAMAccountName=' + username + '))'

    # search ldap for username
    results = ld.search(base, scope, filter, attrs)
    resp_type, user = ld.result(results, 60)
    name, attrs = user[0]

    # check if user is in IT group
    if hasattr(attrs, 'has_key') and 'memberOf' in attrs:
        memberof = attrs['memberOf']
        group = 'CN=' + grp + ',OU=' + org + ',OU=Groups,OU=Organization,DC=bigelow,DC=org'
        if group in memberof:
            return True

    return False

def get_user_info(full_name):
    # init ldap
    ld = ldap.initialize('ldap://ad01.bigelow.org')
    ld.protocol_version = 3
    ld.set_option(ldap.OPT_REFERRALS, 0)

    # check password - rebind with user
    ld.simple_bind_s('pythonauth'+'@bigelow.org', '4#Btb%>84==2jR3Y6z4Z=Vvke2j{4NVNHKDLcWXrCwhF9a((s>')

    # set up user query
    scope = ldap.SCOPE_SUBTREE
    attrs = ['*']
    base = 'dc=bigelow,dc=org'
    filter = '(&(objectClass=user)(name=' + full_name + '))'

    # search ldap for username
    results = ld.search(base, scope, filter, attrs)
    resp_type, user = ld.result(results, 60)
    name, attrs = user[0]

    return [attrs['sAMAccountName'],attrs['userPrincipalName']]

def member_of(grp):
    if grp is not None:
        # init ldap
        ld = ldap.initialize('ldap://ad01.bigelow.org')
        ld.protocol_version = 3
        ld.set_option(ldap.OPT_REFERRALS, 0)

        # check password - rebind with user
        ld.simple_bind_s('pythonauth'+'@bigelow.org', '4#Btb%>84==2jR3Y6z4Z=Vvke2j{4NVNHKDLcWXrCwhF9a((s>')

        # set up user query
        scope = ldap.SCOPE_SUBTREE
        attrs = ['*']
        base = 'dc=bigelow,dc=org'
        filter = '(&(objectClass=group)(sAMAccountName=' + grp + '))'

        # search ldap for username
        results = ld.search(base, scope, filter, attrs)
        resp_type, user = ld.result(results, 60)
        name, attrs = user[0]
        
        member_list = []
        if hasattr(attrs, 'has_key') and 'member' in attrs:
            members = attrs['member']
            for member in members:
                member_list.append(str(member).replace('=',',').split(',')[1])

        return member_list
    
    return []

def belong_to(username):
    if username is not None:
        # init ldap
        ld = ldap.initialize('ldap://ad01.bigelow.org')
        ld.protocol_version = 3
        ld.set_option(ldap.OPT_REFERRALS, 0)
        
        # check password - rebind with user
        ld.simple_bind_s('pythonauth'+'@bigelow.org', '4#Btb%>84==2jR3Y6z4Z=Vvke2j{4NVNHKDLcWXrCwhF9a((s>')
        
        # set up user query
        scope = ldap.SCOPE_SUBTREE
        attrs = ['*']
        base = 'dc=bigelow,dc=org'
        filter = '(&(objectClass=user)(sAMAccountName=' + username + '))'
        
        # search ldap for username
        results = ld.search(base, scope, filter, attrs)
        resp_type, user = ld.result(results, 60)
        name, attrs = user[0]
        
        group_list = []
        if hasattr(attrs, 'has_key') and 'memberOf' in attrs:
            groups = attrs['memberOf']
            for group in groups:
                group_list.append([str(group).replace('=',',').split(',')[3],
                str(group).replace('=',',').split(',')[1]])
        
        return group_list
    
    return []

# if the user exists, returns usrename, else returns None
def user_exist(username):
    if username is not None:
        try:
            # init ldap
            ld = ldap.initialize('ldap://ad01.bigelow.org')
            ld.protocol_version = 3
            ld.set_option(ldap.OPT_REFERRALS, 0)
        
            # check password - rebind with user
            ld.simple_bind_s('pythonauth'+'@bigelow.org', '4#Btb%>84==2jR3Y6z4Z=Vvke2j{4NVNHKDLcWXrCwhF9a((s>')
        
            # set up user query
            scope = ldap.SCOPE_SUBTREE
            attrs = ['*']
            base = 'dc=bigelow,dc=org'
            filter = '(&(objectClass=user)(sAMAccountName=' + username + '))'
        
            # search ldap for username
            results = ld.search(base, scope, filter, attrs)
            resp_type, user = ld.result(results, 60)
            name, attrs = user[0]
        
            return attrs['sAMAccountName'][0]
        except:
            return None
    return None

# User object communicates with the database to extract user id,
#  email, username, role_id and password_hash
class User(UserMixin, db.Model):
    __tablename__ = 'users'
    __bind_key__ = 'portaldb'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    username = db.Column(db.String(64), unique=True, index=True)
    full_name = db.Column(db.String(64), unique=True, index=True)
    manager = db.Column(db.Integer, unique=True, index=True)
    hire_date = db.Column(db.Integer, unique=True, index=True)
    trainings = db.relationship('Training', backref='user', lazy='dynamic')
    courses = db.relationship('Course', backref='user', lazy='dynamic')
    course_content = db.relationship('CourseContent', backref='user', lazy='dynamic')
    
    def iss_(self, perms):
        for p in perms:
            if check_group(self.username, p[0], p[1]):
                return True
        return False
    
    def is_administrator(self):
        return self.iss_(Permission.ADMINISTER)
    
    # auth by name of group or group of groups
    def is_name(self, perms):
        return self.iss_(eval("Permission."+perms))
    
    def is_(self, organization, group):
        return check_group(self.username, organization, group)
    
    def __repr__(self):
        return '<User %r>' % self.username
    def __init__(self, **kwargs): 
        super(User, self).__init__(**kwargs)

class Expense(db.Model):
    __tablename__ = 'expenses'
    __bind_key__ = 'portaldb'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.String(64), db.ForeignKey('users.id'))
    username = db.Column(db.String(64))
    submit_date = db.Column(db.String(64))
    purchase_date = db.Column(db.String(64))
    description = db.Column(db.String(64))
    vendor = db.Column(db.String(64))
    amount = db.Column(db.String(64))
    account = db.Column(db.Integer)
    receipt = db.Column(db.String(64))
    approved = db.Column(db.Integer)
    
    # approve expense
    def approve(self, password):
        self.approved = True
        
    def __repr__(self):
        return '<Expense %r>' % self.id

class Vendor(db.Model):
    __tablename__ = 'vendors'
    __bind_key__ = 'portaldb'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))

# train = db.Table('train',
#     db.Column('training_id', db.Integer, db.ForeignKey('training.id')),
#     db.Column('user_id', db.Integer, db.ForeignKey('user.id'))
# )

class Training(db.Model):
    __tablename__ = 'training'
    __bind_key__ = 'portaldb'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    course_id = db.Column(db.Integer, db.ForeignKey('courses.id'))
    year = db.Column(db.Integer)
    status = db.Column(db.Integer)
    location = db.Column(db.Integer)
    completed = db.Column(db.String(64))
    # timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    score = db.Column(db.Integer)
    questions = db.Column(db.Integer)
    # user = db.relationship('User', secondary=train, backref=db.backref('training', lazy='dynamic'))
    
    def __repr__(self):
        return '<Training %r>' % self.id

class Course(db.Model):
    __tablename__ = 'courses'
    __bind_key__ = 'portaldb'
    id = db.Column(db.Integer, primary_key=True)
    created_by = db.Column(db.Integer, db.ForeignKey('users.id'))
    updated_by = db.Column(db.Integer)
    name = db.Column(db.String(64))
    url = db.Column(db.String(64))
    parts = db.Column(db.Integer)
    updated = db.Column(db.Integer)
    required = db.Column(db.Integer)
    trainings = db.relationship('Training', backref='course', lazy='dynamic')
    content = db.relationship('CourseContent', backref='course', lazy='dynamic')
    
    def length(self):
        return CourseContent.query.filter_by(course_id=self.id).count()
        
    
    def __repr__(self):
        return '<Course %r>' % self.id

    def __init__(self, **kwargs): 
        super(Course, self).__init__(**kwargs)


class CourseContent(db.Model):
    __tablename__ = 'course_content'
    __bind_key__ = 'portaldb'
    id = db.Column(db.Integer, primary_key=True)
    course_id = db.Column(db.Integer, db.ForeignKey('courses.id'))
    created_by = db.Column(db.Integer, db.ForeignKey('users.id'))
    updated_by = db.Column(db.Integer)
    location = db.Column(db.Integer)
    prev = db.Column(db.Integer)
    next = db.Column(db.Integer)
    title = db.Column(db.String(64))
    url = db.Column(db.String(64))
    html = db.Column(db.Text)
    seconds = db.Column(db.Integer)
    score = db.Column(db.Integer)
    total = db.Column(db.Integer)

    def __repr__(self):
        return '<CourseContent %r>' % self.id



class TransactionRequest(db.Model):
    __tablename__ = 'transaction_request'
    __bind_key__ = 'db2'

    id = db.Column(db.Integer, primary_key=True)
    x_fp_timestamp = db.Column(db.Integer)
    x_amount = db.Column(db.Integer)
    x_first_name = db.Column(db.String(50))
    x_last_name = db.Column(db.String(50))
    x_email = db.Column(db.String(50))
    x_address = db.Column(db.String(60))
    x_city = db.Column(db.String(40))
    x_state = db.Column(db.String(2))
    x_zip = db.Column(db.String(10))
    x_country = db.Column(db.String(60))
    x_phone = db.Column(db.String(25))
    acknowledgement = db.Column(db.String(100))
    gift_designation = db.Column(db.String(60))
    recurring = db.Column(db.Integer)
    arb_period = db.Column(db.Integer)
    arb_end_after = db.Column(db.Integer)
    is_deleted = db.Column(db.Integer)
    anonymous = db.Column(db.Integer)
    subscribe = db.Column(db.Integer)
    gift_designation_type_id = db.Column(db.Integer)
    gift_dedication_type_id = db.Column(db.Integer)
    dedication_name = db.Column(db.String(50))
    
    def __repr__(self):
        return '<TransactionRequest %r>' % self.id



class Jellyfish(db.Model):
    __tablename__ = 'jellyfish'
    __bind_key__ = 'odb'
    id = db.Column(db.Integer, primary_key=True)
    first = db.Column(db.Text)
    last = db.Column(db.Text)
    email = db.Column(db.Text)
    affiliation = db.Column(db.Text)
    date_found = db.Column(db.Date)
    time_found = db.Column(db.Text)
    species = db.Column(db.Text)
    lat = db.Column(db.Float)
    lon = db.Column(db.Float)
    city = db.Column(db.Text)
    state = db.Column(db.Text)
    location_desc = db.Column(db.Text)
    count = db.Column(db.Integer)
    size = db.Column(db.Text)
    comments = db.Column(db.Text)
    image = db.Column(db.Text)
    
    # approve expense
    def approve(self, password):
        self.approved = True
        
    def __repr__(self):
        return '<Jellyfish %r>' % self.id



@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
def load_expenses(user_id):
    return Expense.query.get(int(user_id))


