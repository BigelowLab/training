import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'fs03lh95vc29l3hf84hg'
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'data.sqlite')
        
    SQLALCHEMY_BINDS = {
        'db1': SQLALCHEMY_DATABASE_URI,
        'portaldb': 'postgresql://biouser:B1oT0x1c@blis.bigelow.org/portal_bigelow_org',
        'db2': 'postgresql://biouser:B1oT0x1c@blis.bigelow.org/blisdb'
        # 'odb': 'postgresql://bigelow:TPnduuRHhkcatzg}4TiMBMQxMRD@104.131.161.80/jellyfish_bigelow_org',
    }
    
    # Mail Server
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = 'kguay@bigelow.org'
    MAIL_PASSWORD = 'utgbcukayjdvftnt'
    FLASKY_MAIL_SUBJECT_PREFIX = '[Bigelow Pay]'
    FLASKY_MAIL_SENDER = 'it@bigelow.org'
    
    # LDAP Configuration (from BLIS)
    LDAP_HOST = 'ad01.bigelow.org'
    LDAP_DOMAIN = 'bigelow.org'
    LDAP_SEARCH_BASE = 'OU=Domain Users,DC=bigelow,DC=org'
    LDAP_LOGIN_VIEW = 'login'
    
    LDAP_USER = 'pythonauth'
    LDAP_PASS = '4#Btb%>84==2jR3Y6z4Z=Vvke2j{4NVNHKDLcWXrCwhF9a((s>'
    
    UPLOAD_FOLDER = os.path.join(basedir, 'app/static/training/user_images')
    
    @staticmethod
    def init_app(app):
        pass

# development
class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')

# testing
class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data-test.sqlite')

# production
class ProductionConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data.sqlite')

# dictionary object to define configuration mode (currently runs with default)
config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,

    'default': ProductionConfig
}
