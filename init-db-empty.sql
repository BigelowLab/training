PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;

CREATE TABLE roles (
id INTEGER NOT NULL, 
name VARCHAR(64), "default" BOOLEAN, permissions INTEGER, 
PRIMARY KEY (id), 
UNIQUE (name)
);
INSERT INTO "roles" VALUES(1,'Moderator',0,15);
INSERT INTO "roles" VALUES(2,'Administrator',0,255);
INSERT INTO "roles" VALUES(3,'User',1,7);

CREATE TABLE users (
id INTEGER NOT NULL, 
username VARCHAR(64), 
email VARCHAR(64), 
full_name TEXT, 
passwd_length INTEGER, 
manager INTEGER, 
hire_date TEXT, 
PRIMARY KEY (id)
);

CREATE TABLE courses (
id INTEGER NOT NULL, 
created_by INTEGER,
updated_by INTEGER,
name VARCHAR(64), 
url VARCHAR(64), 
parts INTEGER, 
updated INTEGER, 
required INTEGER, 
PRIMARY KEY (id),
FOREIGN KEY(created_by) REFERENCES users (id),
FOREIGN KEY(updated_by) REFERENCES users (id)
);
INSERT INTO "courses" VALUES(1,'IT Security','training/it_security',8,2015,1);
INSERT INTO "courses" VALUES(2,'Sexual Harassment','training/sexual_harassment',5,2015,1);


CREATE TABLE course_content (
id INTEGER NOT NULL, 
created_by INTEGER,
updated_by INTEGER,
course_id INTEGER,
location INTEGER,
prev INTEGER DEFAULT 0,
next INTEGER DEFAULT 0,
title VARCHAR(64),
url VARCHAR(64),
html TEXT,
seconds INTEGER DEFAULT 0,
score INTEGER DEFAULT 0,
questions INTEGER DEFAULT 0,
PRIMARY KEY (id),
FOREIGN KEY(created_by) REFERENCES users (id),
FOREIGN KEY(updated_by) REFERENCES users (id),
FOREIGN KEY(course_id) REFERENCES courses (id)
);
INSERT INTO "course_content" VALUES(1,1,1,'first','training/it_security/1.html', 'Hello World!');
INSERT INTO "course_content" VALUES(2,1,2,'second','training/it_security/2.html', 'Hello World!');
INSERT INTO "course_content" VALUES(3,1,3,'third','training/it_security/3.html', 'Hello World!');
INSERT INTO "course_content" VALUES(4,1,4,'fourth','training/it_security/4.html', 'Hello World!');
INSERT INTO "course_content" VALUES(5,1,5,'fifth','training/it_security/5.html', 'Hello World!');
INSERT INTO "course_content" VALUES(6,1,6,'sixth','training/it_security/6.html', 'Hello World!');
INSERT INTO "course_content" VALUES(7,1,7,'seventh','training/it_security/7.html', 'Hello World!');
INSERT INTO "course_content" VALUES(8,1,8,'eighth','training/it_security/8.html', 'Hello World!');
INSERT INTO "course_content" VALUES(9,1,9,'nineth','training/it_security/9.html', 'Hello World!');
INSERT INTO "course_content" VALUES(10,1,10,'tenth','training/it_security/10.html', 'Hello World!');
INSERT INTO "course_content" VALUES(11,1,11,'eleventh','training/it_security/11.html', 'Hello World!');



CREATE TABLE training (
id INTEGER NOT NULL,
user_id INTEGER,
course_id INTEGER,
year INTEGER,
status INTEGER DEFAULT 0,
location INTEGER DEFAULT 1,
score INTEGER DEFAULT 0,
questions INTEGER DEFAULT 0,
completed TEXT,
PRIMARY KEY (id), 
FOREIGN KEY(user_id) REFERENCES users (id),
FOREIGN KEY(course_id) REFERENCES courses (id)
);

INSERT INTO "training" VALUES(1,1,1,2015,1,4,0,0);
INSERT INTO "training" VALUES(2,1,2,2015,3,1,0,0);
INSERT INTO "training" VALUES(3,2,1,2015,0,1,0,0);
INSERT INTO "training" VALUES(4,3,1,2015,2,8,0,0);
INSERT INTO "training" VALUES(5,4,1,2015,8,8,0,0);

